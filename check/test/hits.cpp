#define HITSDEBUG 0

#include "hits.h"
#include "constants.h"
#include "TFile.h"
#include "TVector3.h"
#include "TTree.h"
#include "TMath.h"
//#include "yaml-cpp/yaml.h"
#include <string>
#include <vector>
#include <cmath>
#include <iostream>

using namespace std;


DomHits::DomHits(string datadir, int dataid):datadir(datadir), dataid(dataid){
	LoadData();
	//cout<<"Dom Hits Initialized"<<endl;
}

void DomHits::LoadData(){
    auto f = new TFile( (datadir).c_str() );
    auto t_dom = (TTree*) f->Get("DomHit");
	auto t_track = (TTree*) f->Get("Primary");
	vector<float> *domid=nullptr, *t=nullptr;
	vector<float> *x0=nullptr,*y0=nullptr,*z0=nullptr,*px=nullptr,*py=nullptr,*pz=nullptr;

	t_dom->SetBranchAddress("t0",&t);
	t_dom->SetBranchAddress("DomId",&domid);

	t_track->SetBranchAddress("x0",&x0);
	t_track->SetBranchAddress("y0",&y0);
	t_track->SetBranchAddress("z0",&z0);
	t_track->SetBranchAddress("px",&px);
	t_track->SetBranchAddress("py",&py);
	t_track->SetBranchAddress("pz",&pz);

	t_dom->GetEntry(dataid);
	t_track->GetEntry(dataid);

	/////////////////Track Information//////////////
	TVector3 direction( (*px)[0], (*py)[0], (*pz)[0] );
	parameters.push_back( (*x0)[0] );
	parameters.push_back( (*y0)[0] );
	parameters.push_back( (*z0)[0] );
	parameters.push_back( direction.Theta() );
	parameters.push_back( direction.Phi() );
	////////////////Hit Information//////////////
	hit_num = t->size();
	
	vector<int> domidtmp(domid->begin(),domid->end());
	hit_dom_id = domidtmp;
	vector<double> ttmp(t->begin(),t->end());
	hit_t = ttmp;

	for(int hitid=0; hitid<hit_num; hitid++){
		int nx = hit_dom_id[hitid]/(Dom_Array_Y*Dom_Array_Z);
		int ny = (hit_dom_id[hitid]%(Dom_Array_Y*Dom_Array_Z)) / Dom_Array_Z;
		int nz = (hit_dom_id[hitid]%(Dom_Array_Y*Dom_Array_Z)) % Dom_Array_Z;
		hit_x.push_back( ( nx - (Dom_Array_X-1.)/2. ) * Dom_DX );
		hit_y.push_back( ( ny - (Dom_Array_Y-1.)/2. ) * Dom_DY );
		hit_z.push_back( ( nz - (Dom_Array_Z-1.)/2. ) * Dom_DZ );
	}
	delete f;
}




ZippedDomHits::ZippedDomHits(DomHits& hits){
	int totalhitnum = hits.GetHitNum();
	vector<double> par = hits.GetTruthPar();
	datadir = hits.GetDataDir();
	dataid = hits.GetDataId();
	for(int i=0; i<5; i++) parameters.push_back( par[i] );

	for(int hitid=0; hitid<totalhitnum; hitid++){
		int rep_id=-1;	//locate the replicated dom_id 
		for(int domid=0; domid<hit_num; domid++)
			if(hit_dom_id[domid] == hits.GetHitDomId(hitid)){
				rep_id = domid;
				break;
			}
		if(rep_id==-1){
			hit_x.push_back( hits.GetHitX(hitid) );
			hit_y.push_back( hits.GetHitY(hitid) );
			hit_z.push_back( hits.GetHitZ(hitid) );
			hit_t.push_back( hits.GetHitT(hitid) );
			hit_dom_id.push_back( hits.GetHitDomId(hitid) );
			hit_zipped_num.push_back(1);
			hit_num += 1;
		}
		else{
			hit_t[rep_id] = hit_t[rep_id]<hits.GetHitT(hitid)? hit_t[rep_id]:hits.GetHitT(hitid);
			hit_zipped_num[rep_id] += 1;
		}
	}
}


double ResTime(vector<double> par, double hit[4]){
/* Par:
 *  pos0: position of vertex
 *  n0: direction of trail
 *  dom_pos: position of DOM hit
 *  dom_t: time of DOM hit
 *
 * Return:
 *  residual time
 */

    double res_time = 0, dom_t=hit[0];
	TVector3 r0(par[0],par[1],par[2]),
			 ri(hit[1],hit[2],hit[3]),
			 n(TMath::Sin(par[3])*TMath::Cos(par[4]), TMath::Sin(par[3])*TMath::Sin(par[4]), TMath::Cos(par[3]));

	TVector3 deltar = ri - r0;
    n = 1./n.Mag() * n;
    double theta = n.Angle(deltar);
    double l = abs(deltar.Mag() * TMath::Cos(theta));
    double d = abs(deltar.Mag() * TMath::Sin(theta));

    res_time = dom_t - (d/(c_n*sinth) + (l - d/tanth) / c - Pmt_R/c_n);
	return res_time;

}


//void ReadYaml(std::string filedir, vector<double> &par){
///*
// *  Par:
// *  filedir: location of yaml file
// *  par: Truth parameters of this event: x,y,z,theta,phi
// */
//	filedir = filedir+"/particle.yaml";
//	TVector3 direction;
//	YAML::Node config;
//	config = YAML::LoadFile(filedir);
//	for(int i=0; i<3; i++){
//	    par.push_back( config["position"][i].as<double>() );
//	    direction[i] = config["direction"][i].as<double>();
//    }
//	par.push_back( direction.Theta() );
//	par.push_back( direction.Phi() );
//}



double GetDis(std::vector<double> par, double hit[4]){
	TVector3 ri (hit[1],hit[2],hit[3] );
	TVector3 r0 (par[0],par[1],par[2] );
	TVector3 n;
	n.SetMagThetaPhi(1,par[3],par[4]);
	double distance = TMath::Sqrt( pow((ri-r0).Mag(),2) - pow( (ri-r0)*n,2) );
	return distance;
}

#if HITSDEBUG
int main(){
	DomHits hits("/lustre/collider/mocen/project/hailing/data/sea_water_1TeV_10000.root",0);
	cout<<"In Dom Hits:"<<endl;
	cout<<hits.GetDataDir()<<endl;
	cout<<"data id: "<<hits.GetDataId()<<endl;

	cout<<"size: "<<hits.GetHitNum()<<endl;
	vector<double> truthpar = hits.GetTruthPar();
	cout<<"pars: ";
	for(int i=0; i<5; i++) cout<<truthpar[i]<<" ";
	cout<<endl;

	ZippedDomHits zipped_hits(hits);
	cout<<"In Zipped Hits:"<<endl;
	cout<<"size: "<<zipped_hits.GetHitNum()<<endl;
	truthpar = zipped_hits.GetTruthPar();
	cout<<"pars: ";
	for(int i=0; i<5; i++) cout<<truthpar[i]<<" ";
	cout<<endl;
}
#endif
