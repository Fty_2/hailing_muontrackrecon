#define MUONTRACKDEBUG 0
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnUserParameterState.h"
#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnMigrad.h"
#include "TVector3.h"

#include "hits.h"
#include "MuonTrack.h"
#include "FCN.h"

using namespace std;
using namespace ROOT::Minuit2;

MuonTrack::MuonTrack(DomHits* dom_hits):dom_hits(dom_hits){
//	cout<<"Muon Track Initialized"<<endl;
	vector<double> par = dom_hits->GetTruthPar();
	for(int i=0;i<3;i++){
		truth_vertex.push_back(par[i]);
		recon_vertex.push_back(0);
	}
	for(int i=0;i<2;i++){
		truth_angle.push_back(par[3+i]);
		recon_angle.push_back(0);
	}
}


void MuonTrack::ReconLineFit(){
	double v[3]={0};
	double means[8]={0};  //0:tmean 1:xmean 2:ymean 3:zmean 4:t2mean 5:txmean 6:tymean 7:tzmean
	for(int hitid=0; hitid<dom_hits->GetHitNum(); hitid++){
		double hits[4] = {dom_hits->GetHitT(hitid),dom_hits->GetHitX(hitid),dom_hits->GetHitY(hitid),dom_hits->GetHitZ(hitid)};
		for(int i=0; i<4; i++){
			means[i] += hits[i];
			means[i+4] += hits[0]*hits[i];
		}
	}
	for(int i=0; i<8; i++)
		means[i] = means[i]/dom_hits->GetHitNum();
	
	for(int i=0; i<3; i++)
		v[i] = (means[5+i] - means[i+1]*means[0]) / (means[4] - means[0]*means[0]);
	double v_norm = sqrt( pow(v[0],2) + pow(v[1],2) + pow(v[2],2) );
	
	TVector3 direct(v[0]/v_norm, v[1]/v_norm, v[2]/v_norm);
	for(int i=0; i<3; i++){
		recon_vertex[i] = means[i+1] - v[i]*means[0];
	}
	recon_angle[0] = direct.Theta();
	recon_angle[1] = direct.Phi();
	linefit_flag = true;
}


void MuonTrack::ReconMes(){
	//////////Set Initial Parameters////////
	if(linefit_flag == false)
		ReconLineFit();
	//////////Mes Reconstruction////////
	MesFcn mes_fcn(dom_hits);
	MnUserParameters mes_par;
	mes_par.Add("x",recon_vertex[0],0.1);
	mes_par.Add("y",recon_vertex[1],0.1);
	mes_par.Add("z",recon_vertex[2],0.1);
	mes_par.Add("theta",recon_angle[0],0.0001,0,TMath::Pi());
	mes_par.Add("phi",recon_angle[1],0.0001,-1*TMath::Pi(),TMath::Pi());

	MnMigrad mes_grad(mes_fcn,mes_par);
	FunctionMinimum mes_min = mes_grad();
	for(int i=0;i<3;i++) recon_vertex[i] = mes_min.UserParameters().Value(i);
	for(int i=0;i<2;i++) recon_angle[i] = mes_min.UserParameters().Value(3+i);
	mes_flag = true;
}

void MuonTrack::ReconSpe(){
	//////////Set Initial Parameters////////
	if(linefit_flag == false)
		ReconLineFit();
	//////////Mes Reconstruction////////
	SPEFcn spe_fcn(dom_hits);
	MnUserParameters spe_par;
	spe_par.Add("x",recon_vertex[0],0.1);
	spe_par.Add("y",recon_vertex[1],0.1);
	spe_par.Add("z",recon_vertex[2],0.1);
	spe_par.Add("theta",recon_angle[0],0.0001,0,TMath::Pi());
	spe_par.Add("phi",recon_angle[1],0.0001,-1*TMath::Pi(),TMath::Pi());

	MnMigrad spe_grad(spe_fcn,spe_par);
	FunctionMinimum spe_min = spe_grad();
	for(int i=0;i<3;i++) recon_vertex[i] = spe_min.UserParameters().Value(i);
	for(int i=0;i<2;i++) recon_angle[i] = spe_min.UserParameters().Value(3+i);

	spe_flag = true;
}

void MuonTrack::ReconMpe(){
	//////////Set Initial Parameters////////
	if(mes_flag == false)
		ReconMes();
	//////////Mes Reconstruction////////
	ZippedDomHits* zipped_dom_hits = new ZippedDomHits(*dom_hits);	
	MPEFcn mpe_fcn(zipped_dom_hits);
	MnUserParameters mpe_par;
	mpe_par.Add("x",recon_vertex[0],0.1);
	mpe_par.Add("y",recon_vertex[1],0.1);
	mpe_par.Add("z",recon_vertex[2],0.1);
	mpe_par.Add("theta",recon_angle[0],0.0001,0,TMath::Pi());
	mpe_par.Add("phi",recon_angle[1],0.0001,-1*TMath::Pi(),TMath::Pi());

	MnMigrad mpe_grad(mpe_fcn,mpe_par);
	FunctionMinimum mpe_min = mpe_grad();
	for(int i=0;i<3;i++) recon_vertex[i] = mpe_min.UserParameters().Value(i);
	for(int i=0;i<2;i++) recon_angle[i] = mpe_min.UserParameters().Value(3+i);

	mpe_flag = true;
	delete zipped_dom_hits;
}


double MuonTrack::GetVertexDeviation(){
	TVector3 truth(truth_vertex[0],truth_vertex[1],truth_vertex[2]),
			 recon(recon_vertex[0],recon_vertex[1],recon_vertex[2]);
	return (truth - recon).Mag();
}

double MuonTrack::GetAngleDeviation(){
	TVector3 truth, recon;
	truth.SetMagThetaPhi(1,truth_angle[0],truth_angle[1]);
	recon.SetMagThetaPhi(1,recon_angle[0],recon_angle[1]);

	double radian_dev = recon.Angle(truth);
	double degree_dev = radian_dev*180/TMath::Pi();
	double minute_dev = degree_dev * 60;

	return minute_dev;
}

#if MUONTRACKDEBUG
int main(int argc, char *argv[]){
	
	string id=argv[1];
	DomHits hits("/lustre/collider/mocen/project/hailing/data/muon_samples_ll/"+id);
	MuonTrack track(&hits);
	
	track.ReconLineFit();
	printf("vertex deviation: %f, angle deviation: %f\n",track.GetVertexDeviation(), track.GetAngleDeviation());
	track.ReconMes();
	printf("vertex deviation: %f, angle deviation: %f\n",track.GetVertexDeviation(), track.GetAngleDeviation());
	track.ReconMpe();
	printf("vertex deviation: %f, angle deviation: %f\n",track.GetVertexDeviation(), track.GetAngleDeviation());
	return 0;
}
#endif
