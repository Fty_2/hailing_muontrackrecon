#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TVector3.h"

#include "hits.h"
#include "MuonTrack.h"
#include "FCN.h"
#include "constants.h"

using namespace std;

#define NMETHOD 4
// void ReconTrack(int DataId){
// 	string DataDir = "sea_water_1TeV_10000.root";
// 	DomHits hits(DataDir,DataId);
// 	MuonTrack track(&hits);
// 	track.ReconLineFit();
// 	printf("Line Fit: \n AngleDeviation: %f \n VertexDeviation %f \n",track.GetAngleDeviation(),track.GetVertexDeviation());
// 	track.ReconMes();
// 	printf("M-estimate: \n AngleDeviation: %f \n VertexDeviation %f \n",track.GetAngleDeviation(),track.GetVertexDeviation());
// 	track.ReconSpe();
// 	printf("SPE: \n AngleDeviation: %f \n VertexDeviation %f \n",track.GetAngleDeviation(),track.GetVertexDeviation());
// 	track.ReconMpe();
// 	printf("MPE: \n AngleDeviation: %f \n VertexDeviation %f \n",track.GetAngleDeviation(),track.GetVertexDeviation());
// }



void TestRecon(){
	string DataDir = "/lustre/collider/mocen/project/hailing/data/sea_water_1TeV_10000_sum.root";	
	string method[NMETHOD] = {"LineFit","Mestimate","SPE","MPE"};

	double angle_dev[NMETHOD] = {0};
	double vertex_dev[NMETHOD] = {0};
	
	TFile *f = new TFile("MuonReconV2Result.root","RECREATE");
	TTree *tangle = new TTree("AngleDeviation","AngleDeviation");
	TTree *tvertex = new TTree("VertexDeviation","VertexDeviation");
	for(int i=0; i<NMETHOD; i++){
		tangle->Branch(method[i].c_str(), angle_dev+i, (method[i]+"/D").c_str() );
		tvertex->Branch(method[i].c_str(), vertex_dev+i, (method[i]+"/D").c_str() );
	}

	for(int DataId=0; DataId<10000; DataId++){
		DomHits hits( DataDir,DataId );
		MuonTrack track(&hits);
		
		track.ReconLineFit();
		angle_dev[0]=track.GetAngleDeviation();
		vertex_dev[0]=track.GetVertexDeviation();

		track.ReconMes();
		angle_dev[1]=track.GetAngleDeviation();
		vertex_dev[1]=track.GetVertexDeviation();

		auto tmpv=track.GetReconAngle();
		TVector3 vm, vs;
		vm.SetMagThetaPhi(1,tmpv[0],tmpv[1]);
		
		track.ReconSpe();

		tmpv=track.GetReconAngle();
		vs.SetMagThetaPhi(1,tmpv[0],tmpv[1]);
		if( vs.Angle(vm)>0.5 ){
			track.ReconLineFit();
			track.ReconSpe();
		}
		
		angle_dev[2]=track.GetAngleDeviation();
		vertex_dev[2]=track.GetVertexDeviation();

		// track.ReconMpe();
		// angle_dev[3]=track.GetAngleDeviation();
		// vertex_dev[3]=track.GetVertexDeviation();

		cout<<"Data "<<DataId<<" end;"<<endl;
if(angle_dev[2]>1000|| angle_dev[0]>1000) printf("linefit deviation:%f, spe deviation:%f in data %i\n",angle_dev[0],angle_dev[2],DataId);
		tangle->Fill();
		tvertex->Fill();
	}
	f->cd();
	tangle->Write();
	tvertex->Write();
	f->Close();

	delete f;
	delete tangle;
	delete tvertex;

}

void GetResTimeDistribution(){
//	string DataDir = "/lustre/collider/mocen/project/hailing/data/muon_samples_ll";	
	string DataDir = "/lustre/collider/mocen/project/hailing/data/sea_water_1TeV_10000.root";
//	TH1F* h = new TH1F("ResTime","ResTime",50,-5,20);
//	TH1F* hmin = new TH1F("MinResTime","MinResTime",50,-5,20);
	
	TH1F* h = new TH1F("ResTime","ResTime",20,0,100);
	TH1F* hmin = new TH1F("MinResTime","MinResTime",20,0,100);
	for(int DataId=0; DataId<1000; DataId++){
		DomHits hits( DataDir, DataId );
		ZippedDomHits zippedhits(hits);

		for(int i=0; i<hits.GetHitNum(); i++){
			h->Fill(hits.GetTruthResTime(i));
		}
		for(int i=0; i<zippedhits.GetHitNum();i++){
			//if(zippedhits.GetHitZippedNum(i)>10)
			hmin->Fill(zippedhits.GetTruthResTime(i));
		}
	}
	TFile* f=new TFile("ResTime.root","RECREATE");
	h->Write();
	hmin->Write();
	f->Close();

	delete h;
	delete hmin;
	delete f;
	
}
int main(int argc, char *argv[]){
//	ReconTrack();
	TestRecon();
//	GetResTimeDistribution();
	return 0;
}

