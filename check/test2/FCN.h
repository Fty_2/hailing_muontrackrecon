#ifndef FCN_H
#define FCN_H
#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TMath.h"
#include "TF1.h"
#include "Minuit2/FCNBase.h"

#include "hits.h"
#include "constants.h"

class MesFcn : public ROOT::Minuit2::FCNBase{
public:
	MesFcn(DomHits* dom_hits) :
	theErrorDef(1.),
	hits(dom_hits){
		entries = dom_hits->GetHitNum();
	}

	~MesFcn(){}

	virtual double Up() const {return theErrorDef;}

    virtual double operator()(const std::vector<double>& par) const{
	/*
	 * Par:
	 *  par: trail x0, y0, z0, theta, phi
	 * 
	 * Return:
	 *  M-estimator likelihood: -logL = -Sum(2-2*sqrt(1+t_res*t_res/2))
	 */
		double t_res=0;
		double logl=0;

		for(int entryid=0; entryid<entries; entryid++){
			t_res = hits->GetResTime(par, entryid);
			logl += -(2 - 2*TMath::Sqrt(1 + t_res*t_res/2));
		}
//std::cout<<"MES: "<<logl<<std::endl;
		return logl;
	}
	

private:
	double theErrorDef = 1.;
	DomHits *hits;
	int entries = 0;
};


class SPEFcn : public ROOT::Minuit2::FCNBase{
/*
 * Par:
 *  ziped hits
 *  and so on
 */
public:
	SPEFcn(DomHits* hits) :
	hits(hits),
	// mean_vs_tau(in_mean_vs_tau),
	// tau_vs_distance(in_tau_vs_distance),
	// alpha(in_alpha),
	theErrorDef(1.){
		entries = hits->GetHitNum();
	}

	~SPEFcn(){}

	virtual double Up() const {return theErrorDef;}

    virtual double operator()(const std::vector<double>& par) const{
	/*
	 * Par:
	 *  par: trail x0, y0, z0, theta, phi
	 * 
	 * Return:
	 *  SPE likelihood: -logL = -Sum(g)
	 */
		double t_res=0;
		double distance=0;
		double logl=0;

		for(int entryid=0; entryid<entries; entryid++){
			distance = hits->GetDistance(par,entryid);
			t_res = hits->GetResTime(par, entryid);
			
			// if( t_res<10 ){
			if(1){
				double mean = 0.0199 * distance - 0.114;
				double tau = 0.0669 * distance + 0.328;
				double alpha = 0.0127 * distance + 0.538;
				double time_ = (t_res - mean) / tau;
				double t_sk = alpha * time_;
				
				double log_pdf = log( 2/ (exp(-t_sk)+1) / ( TMath::Pi()*tau*(time_*time_+1) ) );

				logl = logl - log_pdf;
			}
		}
//std::cout<<"SPE: "<<logl<<std::endl;
		return logl;
	}
	

private:
	DomHits* hits;
	double mean_vs_tau = 0.;
	double tau_vs_distance = 0.;
	double alpha = 0.;
	double theErrorDef = 1.;
	int entries = 0;
};

class MPEFcn : public ROOT::Minuit2::FCNBase{
public:
	MPEFcn(ZippedDomHits* zipped_hits) :
	zipped_hits(zipped_hits),
	theErrorDef(1.){
		entries = zipped_hits->GetHitNum();
		pdf = new TF1( "pdf", "2/( exp( [2]*(x-[0])/[1] )+1 )/ ( pi*[1]*( ((x-[0])/[1])^2 + 1 ) ) ",-10,100 );
	}

	~MPEFcn(){}

	virtual double Up() const {return theErrorDef;}

    virtual double operator()(const std::vector<double>& par) const{
		double t_res=0;
		double distance=0;
		double logl=0;

		for(int entryid=0; entryid<entries; entryid++){
			int hit_zipped_num = zipped_hits->GetHitZippedNum(entryid);
			distance = zipped_hits->GetDistance(par,entryid);
			t_res = zipped_hits->GetResTime(par, entryid);

			// if( t_res<10 && hit_zipped_num>10 ){
			if(1){
				double mean = 0.0199 * distance - 0.114;
				double tau = 0.0669 * distance + 0.328;
				double alpha = 0.0127 * distance + 0.538;
//printf("mean:%f, tau:%f, alpha:%f\n",mean,tau,alpha);
				// double time_ = (t_res - mean) / tau;
				// double t_sk = alpha * time_;
				
				pdf->SetParameters(mean,tau,alpha);
				double log_pdf = log( pdf->Eval(t_res) );
				double log_cdf = log( pdf->Integral(-50,t_res) );
				//double log_pdf = log( 2/ (exp(-t_sk)+1) / ( TMath::Pi()*tau*(time_*time_+1) ) );
				//double log_cdf = log( 1./2 - 1./TMath::Pi()*( 1./sqrt(t_sk*t_sk+1)+TMath::ATan(t_sk) ) );
printf("log_pdf:%f, log_cdf:%f\n",log_pdf,log_cdf);
				logl = logl - ( log(hit_zipped_num) + log_pdf + (hit_zipped_num - 1)*log_cdf );
				
			}
		}
		return logl;
	}
	

private:
	ZippedDomHits* zipped_hits;
	TF1* pdf;
	double theErrorDef = 1.;
	int entries = 0;
};

#endif
