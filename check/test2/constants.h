#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <cmath>

	const double c = 0.2998;
	const double n = 1.375;
	const double c_n = c/n;
	const double costh = 1/n;
	const double tanth = sqrt(1 - costh*costh) / costh;
	const double sinth = costh * tanth;

	const int Dom_Array_X = 10;
	const int Dom_Array_Y = 10;
	const int Dom_Array_Z = 50;
	const double Dom_DX = 100;
	const double Dom_DY = 100;
	const double Dom_DZ = 20;
	const double Pmt_R = 0.2;
#endif

