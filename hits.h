#ifndef HITS_H
#define HITS_H
#include <string>
#include <vector>

double ResTime(std::vector<double> par, double hit[4]);

//void ReadYaml(std::string filedir, std::vector<double> &par);

double GetDis(std::vector<double> par, double hit[4]);



class DomHits{
/*
 * Record Every Dom Hits
 */

public:
	DomHits(std::string datadir, int dataid);
	DomHits(DomHits& hits, std::string option="min");
	~DomHits(){};

	void CutResTime(double max_res=100);
	std::string GetDataDir() {return datadir;}
	int GetHitNum() {return hit_num;}
	std::vector<double> GetTruthPar() {return parameters;}
	double GetHitT(int id) {return hit_t[id];}
	double GetHitX(int id) {return hit_x[id];}
	double GetHitY(int id) {return hit_y[id];}
	double GetHitZ(int id) {return hit_z[id];}
	int GetHitDomId(int id) {return hit_dom_id[id];}
	int GetDataId(){return dataid;}
	int GetDomHitdNum(int id) {	return dom_hit_num[id];	}

	double GetResTime(std::vector<double> par, int id) {
		double hit_info[4] = {hit_t[id], hit_x[id], hit_y[id], hit_z[id]};
		return ResTime(par, hit_info);
	}
	double GetTruthResTime(int id) {
		return GetResTime(parameters, id);
	}

	double GetDistance(std::vector<double> par, int id){
		double hit_info[4] = {hit_t[id],hit_x[id],hit_y[id],hit_z[id]};
		return GetDis(par, hit_info);
	}

private:
	void LoadData();

	int hit_num=0;
	std::string datadir=".";
	std::vector<double> hit_x, hit_y, hit_z, hit_t;
	std::vector<int> hit_dom_id, dom_hit_num;
	std::vector<double> parameters;
	int dataid;
	
};





#endif
