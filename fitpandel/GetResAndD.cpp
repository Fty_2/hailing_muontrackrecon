#include "TTree.h"
#include "../hits.cpp"


void GetResAndD(){
    string DataDir = "/lustre/collider/mocen/project/hailing/data/sea_water_1TeV_10000.root";
    TTree* t=new TTree("res_d","res_d");
    double restime=0., d=0.;

    t->Branch("restime",&restime,"restime/D");
    t->Branch("distance",&d,"distance/D");

    for(int DataId=0; DataId<1000; DataId++){
        DomHits hits(DataDir, DataId);
        vector<double> pars = hits.GetTruthPar();
        for(int i=0; i<hits.GetHitNum(); i++){
            restime = hits.GetTruthResTime(i);
            d = hits.GetDistance(pars,i);
            t->Fill();
        }
        cout<<"Data "<<DataId<<" done."<<endl;
    }
    TFile* f=new TFile("res_d.root","RECREATE");
    t->Write();
}