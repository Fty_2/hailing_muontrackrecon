# Compiler: g++ for C++
CC=g++

# Some flags for compilation
CFLAGS=-g -Wall

# Root-related library and include directory
ROOTDEP=`root-config --cflags` `root-config --libs` -lMinuit2

# Yaml-related library and include directory
YAMLDEP=-std=c++17 -I/cvmfs/sft.cern.ch/lcg/views/LCG_97rc4python3/x86_64-centos7-gcc9-opt/include -L/cvmfs/sft.cern.ch/lcg/views/LCG_97rc4python3/x86_64-centos7-gcc9-opt/lib -lyaml-cpp

# Main command
analysis:  
	${CC}  hits.cpp MuonTrack.cpp TestAlgo.cpp ${ROOTDEP} ${CFLAGS} -o main

#analysis:  
#	${CC}  hits.cpp ${ROOTDEP} ${YAMLDEP} ${CFLAGS} -o test
