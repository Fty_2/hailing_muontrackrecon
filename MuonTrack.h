#ifndef MUONTRACK_H
#define MUONTRACK_H
#include <vector>
#include <iostream>
#include <string>
#include "hits.h"

class MuonTrack{

public:
	MuonTrack(DomHits* dom_hits);
	~MuonTrack(){}


	std::vector<double> GetTruthVertex() {return truth_vertex;}
	std::vector<double> GetTruthAngle() {return truth_angle;}
	std::vector<double> GetReconVertex() {return recon_vertex;}
	std::vector<double> GetReconAngle() {return recon_angle;}
	double GetVertexDeviation();
	double GetAngleDeviation();

	void ReconLineFit();
	void ReconMes();
	void ReconSpe(double pars[3]);
	void ReconMpe();

	bool linefit_flag=false, mes_flag=false,spe_flag=false, mpe_flag=false;
	
private:
	std::vector<double> truth_vertex, truth_angle;	//x,y,z; theta,phi
	std::vector<double> recon_vertex, recon_angle;
	DomHits* dom_hits, first_hits;
	
};

#endif
