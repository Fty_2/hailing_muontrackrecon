#ifndef FCN_H
#define FCN_H
#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TMath.h"
#include "TF1.h"
#include "Minuit2/FCNBase.h"

#include "hits.h"
#include "constants.h"

class MesFcn : public ROOT::Minuit2::FCNBase{
public:
	MesFcn(DomHits* dom_hits) :
	theErrorDef(1.),
	dom_hits(dom_hits){
		entries = dom_hits->GetHitNum();
	}

	~MesFcn(){}

	virtual double Up() const {return theErrorDef;}

    virtual double operator()(const std::vector<double>& par) const{
	/*
	 * Par:
	 *  par: trail x0, y0, z0, theta, phi
	 * 
	 * Return:
	 *  M-estimator likelihood: -logL = -Sum(2-2*sqrt(1+t_res*t_res/2))
	 */
		double t_res=0;
		double logl=0;

		for(int entryid=0; entryid<entries; entryid++){
			t_res = dom_hits->GetResTime(par, entryid);
			logl += -(2 - 2*TMath::Sqrt(1 + t_res*t_res/2));
		}
		return logl;
	}
	

private:
	double theErrorDef = 1.;
	DomHits *dom_hits;
	int entries = 0;
};


class SPEFcn : public ROOT::Minuit2::FCNBase{
/*
 * Par:
 *  ziped hits
 *  and so on
 */
public:
	SPEFcn(DomHits* hits,
			double in_mean_vs_tau,
			double in_tau_vs_distance,
			double in_alpha) :
	hits(hits),
	mean_vs_tau(in_mean_vs_tau),
	tau_vs_distance(in_tau_vs_distance),
	alpha(in_alpha),
	theErrorDef(1.){
		entries = hits->GetHitNum();
	}

	~SPEFcn(){}

	virtual double Up() const {return theErrorDef;}

    virtual double operator()(const std::vector<double>& par) const{
	/*
	 * Par:
	 *  par: trail x0, y0, z0, theta, phi
	 * 
	 * Return:
	 *  SPE likelihood: -logL = -Sum(g)
	 */
		double t_res=0;
		double distance=0;
		double logl=0;

		for(int entryid=0; entryid<entries; entryid++){
			distance = hits->GetDistance(par,entryid);
			t_res = hits->GetResTime(par, entryid);
			
			double mean = 0.0199 * distance - 0.114;
			double tau = 0.0669 * distance + 0.328;
			double alpha = 0.0127 * distance + 0.538;
			double time_ = (t_res - mean) / tau;
			double t_sk = alpha * time_;
			
			double log_pdf = log( 2./(exp(-t_sk)+1) / ( TMath::Pi()*tau*(time_*time_+1) ) );

			logl = logl - log_pdf;
		}
		return logl;
	}
	

private:
	DomHits* hits;
	double mean_vs_tau = 0.;
	double tau_vs_distance = 0.;
	double alpha = 0.;
	double theErrorDef = 1.;
	int entries = 0;
};

class MPEFcn : public ROOT::Minuit2::FCNBase{
/*
 * Par:
 *  ziped hits
 *  and so on
 */
public:
	MPEFcn(DomHits* hits,
			double in_mean_vs_tau,
			double in_tau_vs_distance,
			double in_alpha) :
	hits(hits),
	mean_vs_tau(in_mean_vs_tau),
	tau_vs_distance(in_tau_vs_distance),
	alpha(in_alpha),
	theErrorDef(1.){
		entries = hits->GetHitNum();
	}

	~MPEFcn(){}

	virtual double Up() const {return theErrorDef;}

    virtual double operator()(const std::vector<double>& par) const{
	/*
	 * Par:
	 *  par: trail x0, y0, z0, theta, phi
	 * 
	 * Return:
	 *  MPE likelihood: -logL = -Sum(g)
	 */
		double t_res=0;
		double distance=0;
		double logl=0;
		TVector3 r0(par[0],par[1],par[2]);
		TVector3 n0;
		n0.SetMagThetaPhi(1,par[3],par[4]);

//		TF1* f = new TF1("f","exp(2-2*sqrt(1+x*x/2))/2.93",-10,10);

		for(int entryid=0; entryid<entries; entryid++){
			TVector3 ri ( hits->GetHitX(entryid),hits->GetHitY(entryid),hits->GetHitZ(entryid) );
			distance = TMath::Sqrt( pow((ri-r0).Mag(),2) - pow( (ri-r0)*n0,2) );

			int hit_zipped_num = hits->GetDomHitdNum(entryid);
			t_res = hits->GetResTime(par, entryid);

			double tau = tau_vs_distance * distance + 1;
			double mean = mean_vs_tau * tau;
			double time_ = (t_res - mean) / tau;
			double t_sk = alpha * time_;
			
			double log_pdf = log( (t_sk/sqrt(t_sk*t_sk+1)+1)/(TMath::Pi()*tau*(time_*time_+1)));
			double log_cdf = log( 1./2 - 1./TMath::Pi()*( 1./sqrt(t_sk*t_sk+1)+TMath::ATan(t_sk) ) );
		
			logl = logl - ( log(hit_zipped_num) + log_pdf + (hit_zipped_num - 1)*log_cdf );
				
		}
		return logl;
	}
	

private:
	DomHits* hits;
	double mean_vs_tau = 0.;
	double tau_vs_distance = 0.;
	double alpha = 0.;
	double theErrorDef = 1.;
	int entries = 0;
};

#endif
